
import java.util.*;

public class Juegos {

    public static Jugador jugador1;
    public static Jugador jugador2;
    public static int partidas;
    public static Scanner keyboard = new Scanner(System.in);

    /**
     * pide nombre de jugador, 1 o 2 crea objeto "Jugador" y lo asigna a jugador1 o
     * jugador2
     */
    public static void pideJugador(int numJugador) {
        // pedimos nombre de jugador
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador);
        System.out.println(pregunta);
        String nombre = keyboard.next();
        Jugador j = new Jugador(nombre);

        if (numJugador == 1) {
            Juegos.jugador1 = j;
        } else {
            Juegos.jugador2 = j;
        }
    }

    /**
     * muestra menu de juegos y pide opción
     */

    public static void menu() {
        System.out.println("*******************");
        System.out.println("JUEGOS DISPONIBLES:");
        System.out.println("*******************");
        System.out.println("1: Cara o Cruz");
        System.out.println("2: Piedra Papel Tijera");
        System.out.println("3: La Ruleta");
        System.out.println("*******************");
        // pedimos opcion de juego, comprobando validez
        int opcion = 0;
        do {
            System.out.print("Introduce juego: ");
            opcion = keyboard.nextInt();
        } while (opcion < 1 || opcion > 3);

        switch (opcion) {
        case 1:
            Juegos.caraCruz();
            break;
        case 2:
            Juegos.piedraPapelTijera();
            break;
        case 3:
            Juegos.ruleta();
            break;
        default:
            break;
        }
    }

    /**
     * juego caraCruz
     */
    public static void caraCruz() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("CARA O CRUZ:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Cara (C) o cruz(X) ?  ");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            boolean ganador = rnd.nextBoolean();
            if (ganador) {
                System.out.println(" Has acertado!");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println(" Lo siento...");
            }
            Juegos.jugador1.partidas++;
        }
        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
        System.out.println(resumen);
    }

    /*
     * Piedra Papel Tijera
     */
    public static void piedraPapelTijera() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("PIEDRA PAPEL O TIJERA:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Piedra (1), Papel(2) o Tijera(3) ?  ");
            // pedimos apuesta aunque no se utiliza

            int jugadaJugador = keyboard.nextInt();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            int jugadaRival = Math.round(rnd.nextInt(3) + 1);
            String ppt = "";
            switch (jugadaRival) {
            case 1:
                ppt = "Piedra";
                break;
            case 2:
                ppt = "Papel";
                break;
            case 3:
                ppt = "Tijera";
                break;
            }
            System.out.println("Tu rival ha jugado " + ppt);
            if (jugadaJugador == 1 && jugadaRival == 1) {
                System.out.println("Empate!");
                Juegos.jugador1.empates++;
            } else if (jugadaJugador == 1 && jugadaRival == 2) {
                System.out.println("Pierdes!");

            } else if (jugadaJugador == 1 && jugadaRival == 3) {
                System.out.println("Ganas!");
                Juegos.jugador1.ganadas++;

            } else if (jugadaJugador == 2 && jugadaRival == 1) {
                System.out.println("Ganas!");
                Juegos.jugador1.ganadas++;

            } else if (jugadaJugador == 2 && jugadaRival == 2) {
                System.out.println("Empate!");
                Juegos.jugador1.empates++;

            } else if (jugadaJugador == 2 && jugadaRival == 3) {
                System.out.println("Pierdes!");

            } else if (jugadaJugador == 3 && jugadaRival == 1) {
                System.out.println("Pierdes!");

            } else if (jugadaJugador == 3 && jugadaRival == 2) {
                System.out.println("Ganas");
                Juegos.jugador1.ganadas++;
            } else if (jugadaJugador == 3 && jugadaRival == 3) {
                System.out.println("Empate!");
                Juegos.jugador1.empates++;
            }
            Juegos.jugador1.partidas++;
        }
        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas, %d empates.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas, Juegos.jugador1.empates);
        System.out.println(resumen);
    }

    /**
     * juego Ruleta
     * 
     * @throws InterruptedException
     */
    public static void ruleta() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("LA RULETA:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        while (Juegos.jugador1.dinero > 0) {

            // pedimos apuesta aunque no se utiliza
            System.out.printf("Tienes %d de saldo, Cuanto quieres apostar? ", Juegos.jugador1.dinero);
            System.out.println(" ");
            int apuesta = 0;
            System.out.println("Par(0)/Impar(1): ");
            int parImpar = keyboard.nextInt();
            System.out.println("Cantidad: ");
            int apuesta1 = keyboard.nextInt();
            System.out.println("Falta(0)/Pasa(1): ");
            int faltaParsa = keyboard.nextInt();
            System.out.println("Cantidad: ");
            int apuesta2 = keyboard.nextInt();
            System.out.println("Rojo(0)/Negro(1): ");
            int rojoNegro = keyboard.nextInt();
            System.out.println("Cantidad: ");
            int apuesta3 = keyboard.nextInt();

            apuesta = apuesta1 + apuesta2 + apuesta3;

            if (apuesta <= Juegos.jugador1.dinero) {
                System.out.println("Vamos a jugar!");
                int tirada = Math.round(rnd.nextInt(37));
                System.out.println("...");
                System.out.println("...");
                System.out.println("...");
                System.out.println("Ha salido: " + tirada);

                if (tirada == 0) {
                    Juegos.jugador1.dinero -= apuesta;
                } else {

                    // resolver par-impar

                    if (tirada % 2 == 0 && parImpar == 0) {
                        Juegos.jugador1.dinero += apuesta1;
                        System.out.println("Ganas! es Par!");

                    } else if (tirada % 2 == 0 && parImpar == 1) {
                        Juegos.jugador1.dinero -= apuesta1;
                        System.out.println("Pierdes! es Par!");
                    } else if (tirada % 2 != 0 && parImpar == 1) {
                        Juegos.jugador1.dinero += apuesta1;
                        System.out.println("Ganas! es Impar!");
                    } else if (tirada % 2 != 0 && parImpar == 0) {
                        Juegos.jugador1.dinero -= apuesta1;
                        System.out.println("Pierdes! es Impar!");
                    }

                    // resolver rojo-negro

                    if (tirada == 1 || tirada == 3 || tirada == 5 || tirada == 7 || tirada == 9 || tirada == 12
                            || tirada == 14 || tirada == 16 || tirada == 18 || tirada == 19 || tirada == 21
                            || tirada == 23 || tirada == 25 || tirada == 27 || tirada == 30 || tirada == 32
                            || tirada == 34 || tirada == 36 && rojoNegro == 0) {
                        Juegos.jugador1.dinero += apuesta2;
                        System.out.println("Ganas! es Rojo!");
                    } else if (tirada == 1 || tirada == 3 || tirada == 5 || tirada == 7 || tirada == 9 || tirada == 12
                            || tirada == 14 || tirada == 16 || tirada == 18 || tirada == 19 || tirada == 21
                            || tirada == 23 || tirada == 25 || tirada == 27 || tirada == 30 || tirada == 32
                            || tirada == 34 || tirada == 36 && rojoNegro == 1) {
                        Juegos.jugador1.dinero -= apuesta2;
                        System.out.println("Pierdes! es Rojo!");
                    } else if (tirada == 2 || tirada == 4 || tirada == 6 || tirada == 8 || tirada == 10 || tirada == 11
                            || tirada == 13 || tirada == 15 || tirada == 17 || tirada == 20 || tirada == 22
                            || tirada == 24 || tirada == 26 || tirada == 28 || tirada == 28 || tirada == 29
                            || tirada == 31 || tirada == 33 || tirada == 35 && rojoNegro == 1) {
                        Juegos.jugador1.dinero += apuesta2;
                        System.out.println("Ganas! es Negro!");
                    } else if (tirada == 2 || tirada == 4 || tirada == 6 || tirada == 8 || tirada == 10 || tirada == 11
                            || tirada == 13 || tirada == 15 || tirada == 17 || tirada == 20 || tirada == 22
                            || tirada == 24 || tirada == 26 || tirada == 28 || tirada == 28 || tirada == 29
                            || tirada == 31 || tirada == 33 || tirada == 35 && rojoNegro == 0) {
                        Juegos.jugador1.dinero -= apuesta2;
                        System.out.println("Pierdes! es Negro!");
                    }

                    // resolver falta-pasa

                    if (tirada < 19 && faltaParsa == 0) {
                        Juegos.jugador1.dinero += apuesta3;
                        System.out.println("Ganas! es Falta!");
                    } else if (tirada < 19 && faltaParsa == 1) {
                        Juegos.jugador1.dinero -= apuesta3;
                        System.out.println("Pierdes! es Falta!");
                    } else if (tirada > 18 && faltaParsa == 1) {
                        Juegos.jugador1.dinero += apuesta3;
                        System.out.println("Ganas! es Parsa!");
                    } else if (tirada > 18 && faltaParsa == 0) {
                        Juegos.jugador1.dinero -= apuesta3;
                        System.out.println("Pierdes! es Parsa!");
                    }
                }

            } else {
                System.out.println("Estas arruinado! PRINGADO");

            }

        }
        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
        System.out.println(resumen);
    }

}
