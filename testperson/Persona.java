package testperson;

/**
 * Persona
 */
public class Persona {

    private String nombre;
    private int edad;

    public String getNombre(){
        return this.nombre;
    }

    public void setNombre(String s){
        this.nombre = s;
    }

    public int getEdad(){
        return this.edad;
    }

    public void setEdad(int i){
        this.edad = i;
    }
}