import java.util.*;

/**
 * TestUtil
 */
public class TestUtil {

    public static void main(String[] args) {

        System.out.println(Util.diaSemana(5, "en"));
        System.out.println(Util.mes(3, "ca"));

        int arrayNum[] = { 2, 5, 6, -1, 4 };

        char arrayChar[] = { 'a', 'u', 'b', '9', 'v' };

        System.out.println(Util.mayor(arrayNum));
        System.out.println(Util.mayor(arrayChar));

        Util.muestraArray(arrayNum, ">>");

        System.out.println(Arrays.toString(Util.pares(arrayNum)));
        System.out.println(Util.password(3));

        System.out.println(Util.verificaEmail("badddda@hotmail.com"));

        System.out.println(Util.encripta("holacaracola", "zola"));
        System.out.println(Util.desencripta("???z|z?z|??z", "zola"));
    }
}