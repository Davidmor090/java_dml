/**
 * Persona
 */
public class Persona {

    String nombre;
    int edad;
    static String ciudad = "Barcelona";

    void saluda() {
        System.out.println("Hola me llamo " + this.nombre + " y tengo " + this.edad + " años.");
    }

    void saluda(Persona otra) {
        if (this.edad > otra.edad) {
            System.out.println("Hola me llamo " + this.nombre + " y soy mayor que " + otra.nombre);

        } else if (this.edad == otra.edad) {
            System.out.println("Hola me llamo " + this.nombre + " y tengo la misma edad que " + otra.nombre);
        } else {
            System.out.println("Hola me llamo " + this.nombre + " y soy más joven que " + otra.nombre);
        }

    }

    void comparar_edad(Persona otra) {

        int diferencia = this.edad - otra.edad;
        if (diferencia > 0) {
            System.out.println("le saco a " + otra.nombre + " " + diferencia + " años");
        } else {
            System.out.println(otra.nombre + " me saca " + diferencia * -1 + " años");
        }

    }

}