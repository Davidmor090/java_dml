import java.util.*;

/**
 * ListaTest
 */
public class ListaTest {

    public static void main(String[] args) {
        
        List<String> lista = new ArrayList<String>();

        lista.add("Barcelona");
        lista.add("Mataro");
        lista.add("Granollers");

        for (String ciudad : lista) {

            System.out.println(ciudad);
            
        }

        lista.remove("Mataro");
        System.out.println(lista);


    }
}