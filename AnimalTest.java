/**
 * AnimalTest
 */
public class AnimalTest {

    public static void main(String[] args) {
        
        Animal toby = new Perro();
        Perro p = (Perro) toby;
        p.ladra();
        p.duerme();

        PerroMutante robot = new PerroMutante();

        System.out.println("patas de perro normal " + p.getNumeroPatas());
        System.out.println("patas de robot " + robot.getNumeroPatas());
    }
}