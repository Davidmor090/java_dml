
import java.util.*;

/**
 * Listas
 */
public class Listas {

    public static void main(String[] args) {

        int[] lista_numeros = { 23, 4, 55, 61, 28, 73, 42, 9 };

        int[] trio = new int[3];

        trio[0] = 11;
        trio[1] = 77;
        trio[2] = 92;

        // for (int numero : lista_numeros) {
        // System.out.println(numero);
        // }

        // String ciudad = "Constantinopla";

        // for (char letra : ciudad.toCharArray()) {
        // System.out.print(letra+"-");
        // }

        System.out.println("Enter string to reverse:");

        Scanner read = new Scanner(System.in);
        String str = read.nextLine();
        String reverse = "";

        for (int i = str.length() - 1; i >= 0; i--) {
            reverse = reverse + str.charAt(i);
        }

        System.out.println("Reversed string is:");
        System.out.println(reverse);
    }

}