
/**
 * InfoNums
 */
public class InfoNums {

    public static void main(String[] args) {

        int total = 0;
        int i = 0;
        int elMayor = Integer.parseInt(args[0]);
        int elMenor = Integer.parseInt(args[0]);

        for (String s : args) {

            i++;
            int num = Integer.parseInt(s);

            if (num > elMayor) {
                elMayor = num;
            }

            if (num <= elMenor) {
                elMenor = num;
            }

            total += num;
        }

        float promedio = (float) total / i;

        System.out.println("hay " + i + " numeros, " + "el mayor es " + elMayor + " el menor es " + elMenor
                + " y su promedio es " + promedio);

    }

}
