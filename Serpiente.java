/**
 * Serpiente
 */
public class Serpiente extends Animal {
    
    public void repta() {
        System.out.println("A reptar!");
    }

    @Override
    public void juega() {
        System.out.println("Estoy jugando...");
    }

}