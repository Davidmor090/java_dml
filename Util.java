import java.util.*;

/**
 * Util
 */
public class Util {

    // DIAS SEMANA Y MESES
    // /////////////////////////////////////////////////////////////////
    static String[] semana_es = { "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sabado", "Domingo" };
    static String[] semana_ca = { "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge" };
    static String[] semana_en = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

    static String[] meses_es = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre",
            "Octubre", "Noviembre", "Diciembre" };
    static String[] meses_ca = { "Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre",
            "Octubre", "Novembre", "Desembre" };
    static String[] meses_en = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
            "October", "November", "December" };

    public static String diaSemana(int i, String s) {

        String resultado = "";
        switch (i) {
        case 1:
            if (s == "es") {
                resultado = semana_es[0];
            } else if (s == "ca") {
                resultado = semana_ca[0];
            } else if (s == "en") {
                resultado = semana_en[0];
            }
            break;
        case 2:
            if (s == "es") {
                resultado = semana_es[1];
            } else if (s == "ca") {
                resultado = semana_ca[1];
            } else if (s == "en") {
                resultado = semana_en[1];
            }
            break;
        case 3:
            if (s == "es") {
                resultado = semana_es[2];
            } else if (s == "ca") {
                resultado = semana_ca[2];
            } else if (s == "en") {
                resultado = semana_en[2];
            }
            break;
        case 4:
            if (s == "es") {
                resultado = semana_es[3];
            } else if (s == "ca") {
                resultado = semana_ca[3];
            } else if (s == "en") {
                resultado = semana_en[3];
            }
            break;
        case 5:
            if (s == "es") {
                resultado = semana_es[4];
            } else if (s == "ca") {
                resultado = semana_ca[4];
            } else if (s == "en") {
                resultado = semana_en[4];
            }
            break;
        case 6:
            if (s == "es") {
                resultado = semana_es[5];
            } else if (s == "ca") {
                resultado = semana_ca[5];
            } else if (s == "en") {
                resultado = semana_en[5];
            }
            break;
        case 7:
            if (s == "es") {
                resultado = semana_es[6];
            } else if (s == "ca") {
                resultado = semana_ca[6];
            } else if (s == "en") {
                resultado = semana_en[6];
            }
            break;

        }
        return resultado;

    }

    public static String mes(int i, String s) {

        String resultado = "";
        switch (i) {
        case 1:
            if (s == "es") {
                resultado = meses_es[0];
            } else if (s == "ca") {
                resultado = meses_ca[0];
            } else if (s == "en") {
                resultado = meses_en[0];
            }
            break;
        case 2:
            if (s == "es") {
                resultado = meses_es[1];
            } else if (s == "ca") {
                resultado = meses_ca[1];
            } else if (s == "en") {
                resultado = meses_en[1];
            }
            break;
        case 3:
            if (s == "es") {
                resultado = meses_es[2];
            } else if (s == "ca") {
                resultado = meses_ca[2];
            } else if (s == "en") {
                resultado = meses_en[2];
            }
            break;
        case 4:
            if (s == "es") {
                resultado = meses_es[3];
            } else if (s == "ca") {
                resultado = meses_ca[3];
            } else if (s == "en") {
                resultado = meses_en[3];
            }
            break;
        case 5:
            if (s == "es") {
                resultado = meses_es[4];
            } else if (s == "ca") {
                resultado = meses_ca[4];
            } else if (s == "en") {
                resultado = meses_en[4];
            }
            break;
        case 6:
            if (s == "es") {
                resultado = meses_es[5];
            } else if (s == "ca") {
                resultado = meses_ca[5];
            } else if (s == "en") {
                resultado = meses_en[5];
            }
            break;
        case 7:
            if (s == "es") {
                resultado = meses_es[6];
            } else if (s == "ca") {
                resultado = meses_ca[6];
            } else if (s == "en") {
                resultado = meses_en[6];
            }
            break;
        case 8:
            if (s == "es") {
                resultado = meses_es[7];
            } else if (s == "ca") {
                resultado = meses_ca[7];
            } else if (s == "en") {
                resultado = meses_en[7];
            }
            break;
        case 9:
            if (s == "es") {
                resultado = meses_es[8];
            } else if (s == "ca") {
                resultado = meses_ca[8];
            } else if (s == "en") {
                resultado = meses_en[8];
            }
            break;
        case 10:
            if (s == "es") {
                resultado = meses_es[9];
            } else if (s == "ca") {
                resultado = meses_ca[9];
            } else if (s == "en") {
                resultado = meses_en[9];
            }
            break;
        case 11:
            if (s == "es") {
                resultado = meses_es[10];
            } else if (s == "ca") {
                resultado = meses_ca[10];
            } else if (s == "en") {
                resultado = meses_en[10];
            }
            break;
        case 12:
            if (s == "es") {
                resultado = meses_es[11];
            } else if (s == "ca") {
                resultado = meses_ca[11];
            } else if (s == "en") {
                resultado = meses_en[11];
            }
            break;

        }
        return resultado;

    }

    // EL
    // MAYOR//////////////////////////////////////////////////////////////////////////
    public static int mayor(int[] i) {

        int resultado = i[0];

        for (int el : i) {

            if (el > resultado) {
                resultado = el;
            }
        }

        return resultado;
    }

    public static char mayor(char[] i) {

        char resultado = i[0];

        for (char el : i) {

            if (el > resultado) {
                resultado = el;
            }
        }

        return resultado;
    }

    // MOSTRAR ARRAY
    // ///////////////////////////////////////////////////////////////////
    public static void muestraArray(int[] i, String s) {

        String resultado = "";

        int longString = s.length();

        for (int el : i) {

            resultado += el + s;

        }

        System.out.println(resultado.substring(0, resultado.length() - longString));
    }

    // PARES
    // ///////////////////////////////////////////////////////////////////////////////////
    public static int[] pares(int[] i) {

        int tamanoarray = 0;
        int posiciones = 0;

        for (int el : i) {

            if (el % 2 == 0) {

                tamanoarray++;

            }

        }
        int[] myIntArray = new int[tamanoarray];

        for (int j = 0; j < tamanoarray; j++) {

            if (i[j] % 2 == 0) {

                myIntArray[posiciones] = i[j];
                posiciones++;
            }

        }

        return myIntArray;

    }

    // CONTRASEÑA
    // ////////////////////////////////////////////////////////////////////////////
    public static String password(int i) {

        String resultado;
        String alfabeto = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        final int N = alfabeto.length();
        Random r = new Random();

        do {
            resultado = "";
            for (int j = 0; j < i; j++) {

                resultado += alfabeto.charAt(r.nextInt(N));
            }
        } while (!(resultado.matches(".*[a-z].*[A-Z].*[0-9].*")));

        return resultado;
    }

    // VERIFICA EMAIL
    // ///////////////////////////////////////////////////////////////////////
    public static String verificaEmail(String s) {
        String resultado;

        if (s.matches("(...).*@(gmail|hotmail).(com|es)$")) {
            resultado = "es correcto";
        } else {
            resultado = "es incorrecto";
        }

        return resultado;
    }

    // ENCRIPTA Y DESENCRIPTA
    // ////////////////////////////////////////////////////////////////
    public static int diferencia;

    public static String encripta(String s1, String s2) {
        String resultado = "";

        char elMayor = s1.charAt(0);
        char elMenor = s1.charAt(0);

        char[] stringToCharArray1 = s1.toCharArray();
        char[] stringToCharArray2 = s2.toCharArray();

        for (char letra : stringToCharArray2) {
            if (letra > elMayor) {
                elMayor = letra;
            }
            if (letra < elMenor) {
                elMenor = letra;
            }
        }
        diferencia = elMayor - elMenor;

        for (char letra : stringToCharArray1) {

            resultado += (char) (letra + diferencia);
        }

        return resultado;
    }

    public static String desencripta(String s1, String s2) {
        String resultado = "";

        char elMayor = s1.charAt(0);
        char elMenor = s1.charAt(0);

        char[] stringToCharArray1 = s1.toCharArray();
        char[] stringToCharArray2 = s2.toCharArray();

        for (char letra : stringToCharArray2) {
            if (letra > elMayor) {
                elMayor = letra;
            }
            if (letra < elMenor) {
                elMenor = letra;
            }
        }

        for (char letra : stringToCharArray1) {

            resultado += (char) (letra - diferencia);
        }

        return resultado;
    }
}