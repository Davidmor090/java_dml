
/**
 * Adivina
 */
import java.util.*;

public class Adivina {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        Random random = new Random();
        int incognita = random.nextInt(100) + 1;
        int intento = 0;
        boolean tryAgain = false;
        do {
            if (!tryAgain) {
                System.out.printf("Adivina el num: ");
            } else {
                System.out.printf("intenta otra vez: ");
            }
            tryAgain = true;

            try {
                intento = keyboard.nextInt();
                if (intento > incognita) {
                    System.out.printf("Te pasaste: ");
                } else if (intento < incognita) {
                    System.out.printf("Te quedaste corto, ");
                }

            } catch (Exception e) {
                System.out.println("***Dato incorrecto***");
                keyboard.next();
            }

        } while (intento != incognita);
        System.out.println("Lo has adivinado!");
        keyboard.close();

    }

}