/**
 * Perro
 */
public class Perro extends Animal {

    public void ladra() {
        System.out.println("Guau!");
    }

    public int getNumeroPatas() {
        return 4;
    }

    @Override
    public void juega() {
        System.out.println("Estoy jugando...");
    }
}