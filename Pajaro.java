/**
 * Pajaro
 */
public class Pajaro extends Animal {

    public void vuela() {
        System.out.println("A volar!");
    }

    public int getNumeroPatas() {
        return 2;
    }

    @Override
    public void juega() {
        System.out.println("Estoy jugando...");
    }
}