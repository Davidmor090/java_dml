
/**
 * InfoNumsK
 */
import java.util.*;

public class InfoNumsK {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num = 0;
        int i = 0;
        int elMayor = 0;
        int elMenor = 0;

        do {

            System.out.printf("Entra un num: ");

            try {
                num = keyboard.nextInt();
                if (num == 0) {
                    break;
                }
                total += num;
            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
            i++;
            if (num > elMayor) {
                elMayor = num;
            }

            if (num <= elMenor) {
                elMenor = num;
            }
        } while (num > 0);

        float promedio = (float) total / i;

        System.out.println("hay " + i + " numeros, " + "el mayor es " + elMayor + " el menor es " + elMenor
                + " y su promedio es " + promedio);
        keyboard.close();

    }

}