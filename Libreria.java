/**
 * Libreria
 */
public class Libreria {

    static float c= 300_000;

    public int doblar(int numero){
        return numero *= 2;
        
    }

    public int doblaEdad(Persona p1){
        return p1.edad *= 2;
        
    }
}